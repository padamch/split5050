app.factory('ExpensesService', function($http,myconstant) {
    var expenses_data ={};
    return {
        addExpenses:function(data){
            return $http({
                method:'POST',
                url: myconstant.apiURL+'expenses', data: data
            }).then(function(result){
                return result.data;
            }, function(error){
                console.log(error.data);
            });
        },
        getExpenses:function(data){
            return $http({
                method:'GET',
                url: myconstant.apiURL+'expenses',
                params: {user: data.user,year:data.year,month:data.month,client_id:data.client_id}
            }).then(function(result){
                expenses_data = result.data;
                return result.data;
            }, function(error){
                console.log(error.data);
            });
        },
        getExpensesUpdated:function(){
            return expenses_data;
        },
        deleteExpenses:function(data){
            return $http({
                method:'DELETE',
                url: myconstant.apiURL+'expenses/delete',
                data: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'} 
            }).then(function(result){
                return result.data;
            }, function(error){
                console.log(error.data);
            });
        }
    }//end return
 });