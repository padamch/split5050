app.factory('UsersService', function($http,myconstant) {
    return {
        signup:function(data){
            return $http({
                method:'POST',
                url: myconstant.apiURL+'users', data: data
            }).then(function(result){
                return result.data;
            });
        },
        login:function(data){
            return $http({
                method:'POST',
                url: myconstant.apiURL+'users/login', data: data
            }).then(function(result){
                return result.data;
            });
        }
    }//end return
 });