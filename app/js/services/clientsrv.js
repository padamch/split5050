app.factory('ClientService', function($http,myconstant) {
    return {
        createClient:function(data){
            return $http({
                method:'POST',
                url: myconstant.apiURL+'clients', data: data
            }).then(function(result){
                return result.data;
            });
        }
    }//end return
 });