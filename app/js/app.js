"use strict";
var app = angular.module('splitApp', ['ui.router','ngMaterial','angular-storage','angular-jwt']);
app.constant('myconstant',{
	'apiURL':'http://padam.dev/split5050/api/v1/'
});

app.config(function($stateProvider, $urlRouterProvider,jwtInterceptorProvider,$httpProvider){
	$urlRouterProvider.otherwise('/');
	// SEND JWT in every request
	jwtInterceptorProvider.tokenGetter = function(store) {
    	return store.get('jwt');
  	}
  	$httpProvider.interceptors.push('jwtInterceptor');
	$stateProvider
	.state('index',{
		url:'/',
		templateUrl:'templates/index.html'
	})
	.state('home',{
		url:'/home',
		templateUrl:'templates/home.html',
		controller:'HomeCtrl',
		data: {
      		requiresLogin: true
    	}
	})
	.state('info',{
		url:'/info',
		templateUrl:'templates/info.html'
	})
	.state('login',{
		url:'/login',
		templateUrl:'templates/login.html',
		controller:'LoginCtrl'		
	})
	.state('client',{
		url:'/client',
		templateUrl:'templates/client.html',
		controller:'ClientCtrl'
	})
	.state('signup',{
		url:'/signup',
		templateUrl:'templates/signup.html',
		controller:'SignupCtrl'
	})
	.state('home.total_expenses',{
		url:'/total_expenses',
		templateUrl:'templates/total_expenses.html',
		controller:'HomeCtrl'		
	})
	.state('home.expenses',{
		url:'/expenses',
		templateUrl:'templates/expenses.html',
		controller:'ExpensesCtrl'		
	})
	.state('home.addexpenses',{
		url:'/addexpenses',
		templateUrl:'templates/addexpenses.html',
		controller:'ExpensesCtrl'		
	})
	.state('home.logout',{
		url:'/logout',
		controller:'LogoutCtrl'		
	});

}); //end config

app.run(function($rootScope, $state, store, jwtHelper) {
	$rootScope.$on('$stateChangeStart', function(e, to) {
		if (to.data && to.data.requiresLogin) {
			if (!store.get('jwt') || jwtHelper.isTokenExpired(store.get('jwt'))) {
				e.preventDefault();
				$state.go('login');
			}
		}
	});
})