app.controller('ExpensesCtrl', ['$scope','$filter','store','ExpensesService',function($scope,$filter,store,ExpensesService) {  
    $scope.expenses={};
    $scope.ddmmyy =new Date();
    var dt = $scope.ddmmyy;
    $scope.expenses.purchased_date=$filter('date')(dt,'yyyy-MM-dd');
    $scope.addExpenses = function(){
        $scope.expenses.user=store.get('user');
        $scope.expenses.client_id=store.get('client_id');
        console.log($scope.expenses);
        ExpensesService.addExpenses($scope.expenses).then(function(data){
            $scope.response=data;
            console.log($scope.response);
            $scope.expenses={};
        }); 
    }; // add expenses

    // GET EXPENSES 
    var d = new Date();
    var m = (d.getMonth()+1);
    var y = d.getFullYear();
    var u = store.get('user');
    var c = store.get('client_id');
    var data = {user:u,year:y,month:m,client_id:c};
    ExpensesService.getExpenses(data).then(function(data){
        $scope.status=data.status;
        $scope.exp=data.msg;
    });
    //delete expenses
    $scope.deleteExpenses = function(exp,index,expensesId){
        var data = {user:u,id:expensesId};
        ExpensesService.deleteExpenses(data).then(function(data){
            console.log(data);
            if(data.status==1){
                exp.splice(index, 1); //delete element from view
            }

        }); 
    }; // add expenses

    // date options to select
     $scope.mmyy = d;
     $scope.selectDate =function(mmyy){
        y = $filter('date')(mmyy,'yyyy');
        m = $filter('date')(mmyy,'MM');
        data = {user:u,year:y,month:m,client_id:1};
        ExpensesService.getExpenses(data).then(function(data){
            $scope.status=data.status;
            $scope.exp=data.msg;
        });
    } //select date

}]); 