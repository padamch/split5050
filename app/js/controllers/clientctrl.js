app.controller('ClientCtrl', ['$scope','ClientService','store',function($scope, ClientService,store) {  
    $scope.client={};
    $scope.createClient = function(){
        //console.log($scope.user);
        ClientService.createClient($scope.client).then(function(data){
            console.log(data);
            $scope.response=data;
            if(data.status==1){
                $scope.client={};
            }else{
                $scope.response.msg='Oops, client already exists!';
            }
        }); 
    }; 
}]); 