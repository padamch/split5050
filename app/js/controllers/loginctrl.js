app.controller('LoginCtrl', ['$scope','$state','UsersService','store',function($scope, $state,UsersService,store) {  
    $scope.user={};
    $scope.login = function(){
        UsersService.login($scope.user).then(function(data){
        	$scope.response = data;
            if(data.status==1){
	            store.set('jwt',data.jwt);
	            store.set('user',$scope.user.user);
                store.set('client_id',$scope.user.client_id);
	            $state.go('home');
	            console.log(data);
	        }
        }); 
    }; 
}]); 