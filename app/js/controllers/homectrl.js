app.controller('HomeCtrl', ['$scope','$state','store','ExpensesService',function($scope, $state,store,ExpensesService) {  
    $scope.user = store.get('user');
    $scope.logout=function(){
    	store.remove('jwt');
    	store.remove('user');
    	$state.go('index');
    	console.log('Logout!!!!!');
    }

// GET EXPENSES 
    var d = new Date();
    var y = d.getFullYear();
    var month = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    $scope.current_month =month[d.getMonth()];
    $scope.current_year = y;
    var m = (d.getMonth()+1);
    var u = store.get('user');
    var c = store.get('client_id');
    var data = {user:u,year:y,month:m,client_id:c};
    ExpensesService.getExpenses(data).then(function(data){
        // it watches if the value of expenses is changed
        $scope.$watch(ExpensesService.getExpensesUpdated,function(o){
            data = o; //new updated data
            console.log(data);
            if(data.status!=0){
            $scope.status=1;
            $scope.exp=data.msg;
            $scope.total = data.total;
            $scope.usersTotal=data.usersTotal;
            var t = data.total;
            var n = data.usersTotal.length;
            var u = data.usersTotal;
            var p = (t/n);
            $scope.perPerson=p;
            $scope.transaction =[];
            console.log('per person: '+p);
            for(i=0;i<n;i++){
                $scope.transaction.push({'user':u[i].user,'expenses':u[i].total,'status':(u[i].total-p)});
            }
        }

        });
        
        
    });

}]); 