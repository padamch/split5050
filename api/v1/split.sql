/*
This DB is created for split5050 application
*/
DROP DATABASE IF EXISTS split5050;
CREATE DATABASE split5050;
USE split5050;

DROP TABLE IF EXISTS s5_expenses;
DROP TABLE IF EXISTS s5_clients;
DROP TABLE IF EXISTS s5_users;

CREATE TABLE s5_clients(
	id INT NOT NULL AUTO_INCREMENT,
	email VARCHAR(30) UNIQUE,
	created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(id)
)ENGINE=INNODB;

INSERT INTO `s5_clients` (`email`) VALUES
	('padam101@hotmail.com'),
	('padam_pm@hotmail.com'),
	('padamchhantyal@gmail.com');

CREATE TABLE s5_users(
	id INT NOT NULL AUTO_INCREMENT,
	user VARCHAR(25) UNIQUE,
	pass VARCHAR(25),
	role enum('basic', 'admin', 'owner') NOT NULL DEFAULT 'basic',
	client_id INT,
	created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(id),
	FOREIGN KEY(client_id) REFERENCES s5_clients(id) ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=INNODB;

INSERT INTO s5_users(`user`,`pass`,`client_id`) VALUES
	('padam',1,1),
	('sambeg',1,1),
	('mamta',1,1);

CREATE TABLE s5_expenses(
	id INT NOT NULL AUTO_INCREMENT,
	item VARCHAR(50),
	price DECIMAL(8,2), #max 7 digits accurate!
	purchased_date DATE,
	user_id INT,
	client_id INT,
	created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(id),
	FOREIGN KEY(user_id) REFERENCES s5_users(id) ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=INNODB;

