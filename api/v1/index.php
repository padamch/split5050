<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
//require '../vendor/slim/slim/Slim/Slim.php';
/** 
* For libraries that specify autoload information, Composer generates
* a vendor/autoload.php file. You can simply include this file and you will get 
 *autoloading for free.
*/
require_once __DIR__ . '/../../vendor/autoload.php';
require 'config.php';
require 'modal/jwtcheck.php';
\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim(array(
  'debug' => true
));
//CLIENTS
$app->get('/clients','getClients');
$app->get('/clients/:id', 'getClient');
$app->post('/clients','addClients');

//USERS
$app->get('/users','getUsers');
$app->get('/users/:id', 'getUser');
$app->post('/users','addUsers');
$app->post('/users/login','login');

$app->get('/jwt','jwt');
$app->get('/test','test');

// EXPENSES
$app->get('/expenses','getExpenses');
$app->post('/expenses','addExpenses');
$app->delete('/expenses/delete','deleteExpenses');


$app->run();

function test() {
	require './modal/expenses.php';
	$users = new Expenses;
	header('Content-Type: application/json');
	echo json_encode($users->getUsers(1));
	exit;
}

function jwt(){
	$token = array(
	    "iss" => "http://example.org",
	    "aud" => "http://example.com",
	    "iat" => 1356999524,
	    "nbf" => 1357000000
	);
	$jwt = JWT::encode($token, KEY);
	$decoded = JWT::decode($jwt, KEY, array('HS256'));

	print_r($decoded);
}

//CLIENTS
function getClients() {
	require './modal/clients.php';
	$clients = new Clients;
	header('Content-Type: application/json');
	echo $clients->getClients();
	exit;
}

function getClient($id) {
  require './modal/clients.php';
  $client = new Clients;
  header('Content-Type: application/json');
  echo $client->getClient($id);
  exit;
}

function addClients(){
	$request = \Slim\Slim::getInstance()->request();
	$data = json_decode($request->getBody());
	require './modal/clients.php';
	$obj = new Clients;
	header('Content-Type: application/json');
	$arrayObj = $obj->addClients($data);
	if($arrayObj['status']==1){
		$jwt = JWTcheck::createJWT($data->email);
		$response = array(
			"msg"=>$arrayObj['msg'],
			"jwt"=>$jwt,
			"status"=>$arrayObj['status']
		);
		echo json_encode($response);
	}else{
		$response = array(
			"msg"=>$arrayObj['msg'],
			"status"=>$arrayObj['status']
		);
		echo json_encode($response);
	}
	exit;
}

//USERS
function getUsers() {
	require './modal/users.php';
	$users = new Users;
	header('Content-Type: application/json');
	echo $users->getUsers();
	exit;
}

function getUser($id) {
  require './modal/users.php';
  $user = new Users;
  header('Content-Type: application/json');
  echo $user->getUser($id);
  exit;
}

function login(){
	$request = \Slim\Slim::getInstance()->request();
	$data = json_decode($request->getBody());
	require './modal/users.php';
	$obj = new Users;
	header('Content-Type: application/json');
	$arrayObj = $obj->login($data);
	if($arrayObj['status']==1){
		$jwt = JWTcheck::createJWT($data->user);
		$response = array(
			"msg"=>$arrayObj['msg'],
			"jwt"=>$jwt,
			"status"=>$arrayObj['status']
		);
		echo json_encode($response);
	}else{
		$response = array(
			"msg"=>$arrayObj['msg'],
			"status"=>$arrayObj['status']
		);
		echo json_encode($response);
	}
	exit;
}

function addUsers(){
	$request = \Slim\Slim::getInstance()->request();
	$data = json_decode($request->getBody());
	require './modal/users.php';
	$obj = new Users;
	header('Content-Type: application/json');
	$arrayObj = $obj->addUsers($data);
	if($arrayObj['status']==1){
		$jwt = JWTcheck::createJWT($data->user);
		$response = array(
			"msg"=>$arrayObj['msg'],
			"jwt"=>$jwt,
			"status"=>$arrayObj['status']
		);
		echo json_encode($response);
	}else{
		$response = array(
			"msg"=>$arrayObj['msg'],
			"status"=>$arrayObj['status']
		);
		echo json_encode($response);
	}
	exit;
}


function getExpenses() {
	$app = new \Slim\Slim();
	$user = $app->request->params('user');
	$year = $app->request->params('year');
	$month = $app->request->params('month');
	$client_id = $app->request->params('client_id');
	$auth = $app->request->headers->get('Authorization');
	$jwt = str_replace('Bearer ','',$auth);
	if(JWTcheck::checkJWT($jwt)==true){
		require './modal/expenses.php';
		$obj = new Expenses;
		header('Content-Type: application/json');
		$arrayObj = $obj->getExpenses($year,$month,$client_id);
		if($arrayObj['status']==1){
			$response = array(
				"msg"=>$arrayObj['msg'],
				"usersTotal"=>$arrayObj['usersTotal'],
				"total"=>$arrayObj['total'],
				"jwt"=>'output jwt here if needed',
				"status"=>$arrayObj['status']
			);
			echo json_encode($response);
		}else{
			$response = array(
				"msg"=>$arrayObj['msg'],
				"status"=>$arrayObj['status']
			);
			echo json_encode($response);
		}
	}
}

function addExpenses(){
	$app = new \Slim\Slim();
	$auth = $app->request->headers->get('Authorization');
	$jwt = str_replace('Bearer ','',$auth);
	if(JWTcheck::checkJWT($jwt)==true){
		$request = $app->request();
		$data = json_decode($request->getBody());
		require './modal/expenses.php';
		$obj = new Expenses;
		header('Content-Type: application/json');
		echo $obj->addExpenses($data);
		exit;
	}
}

// delete expenses
function deleteExpenses(){
	$app = new \Slim\Slim();
	$auth = $app->request->headers->get('Authorization');
	$jwt = str_replace('Bearer ','',$auth);
	if(JWTcheck::checkJWT($jwt)==true){
		$request = $app->request();
		$data = json_decode($request->getBody());
		header('Content-Type: application/json');
		require './modal/expenses.php';
		$obj = new Expenses;
		header('Content-Type: application/json');
		echo $obj->deleteExpenses($data);
		exit;
	}
}



?>