<?php
require 'database.php';
class Expenses extends Database{	
	public function __construct(){}
	
	/*
	* It returns the expenses detail
	*/
	public function getExpenses($year,$month,$client_id){
		
		$total = $this->getTotal($year,$month,$client_id);
		if($total>'0'){
			$usersTotal = $this->getTotalUsers($year,$month,$client_id);
			$sql = "select e.id, e.purchased_date, u.user, e.price from s5_expenses e, s5_users u ".
			"where e.user_id = u.id and YEAR(purchased_date)=$year and MONTH(purchased_date)=$month and e.client_id=$client_id";		
			try {
				$db =$this->getConnection();
			    $stmt = $db->query($sql);  
			    $data = $stmt->fetchAll(PDO::FETCH_OBJ);
			    $db = null;
			    $rowCount = $stmt->rowCount();
			    return ($rowCount>'0')? array("msg"=> $data,"usersTotal"=>$usersTotal,"total"=>$total,"status"=>1)
			    	:array("msg"=>"No transaction exists!","status"=>0);
		  	} catch(PDOException $e) {
		  		return array("msg"=> $e->getMessage(),"status"=>0);
		  	}
		}else{return array("msg"=>"No transaction exists!","status"=>0);}
	}

	/*
	* total of all users
	**/
	public function getTotal($year, $month,$client_id){
		$sql = "select sum(e.price) as total from s5_expenses e, s5_users u ".
		"where e.user_id = u.id and YEAR(purchased_date)=$year and MONTH(purchased_date)=$month and e.client_id=$client_id";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->execute();
		    $total = $stmt->fetchColumn();  
		    $db = null;
		    return $total;
		  } catch(PDOException $e) {
		    return '{"msg":"'.$e->getMessage().'","status":0}';
		}
	}

	/*
	* get users
	*/
	public function getUsers($client_id){
		$db =$this->getConnection();
		$sql = "select user FROM s5_users where client_id=$client_id";
		try {
		    $stmt = $db->query($sql); 
		    $stmt->execute(); 
		    $users = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);/* Fetch all of the values of the first column */
		    $db = null;
		    return $users;
	  	} catch(PDOException $e) {
	    	return '{"msg":"'.$e->getMessage().'","status":0}';
	  	}
	}
	/*
	* array object of total of specific user
	*/
	public function getTotalUsers($year,$month,$client_id){
		$totalUsers = array();
		$users = $this->getUsers($client_id);
		foreach($users as $user){
			array_push($totalUsers, array('user'=>$user,'total'=>$this->getTotalUser($year,$month,$user)));
		}
		return $totalUsers;
	}

	/*
	* total of specific user
	*/
	public function getTotalUser($year, $month,$user){
		$sql = "select sum(e.price) as total from s5_expenses e, s5_users u ".
		"where e.user_id = u.id and u.user='".$user."' and YEAR(purchased_date)=$year and MONTH(purchased_date)=$month";
		try{
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->execute();
		    $total = $stmt->fetchColumn();  
		    $db = null;
		    return $total;
		} catch(PDOException $e) {
		    return '{"msg":"'.$e->getMessage().'","status":0}';
		}
	}

	/*
	* It adds expenses detail
	*/
	public function addExpenses($data){	
		$user_id = $this->getUserId($data->user);// TODO: check if its true
		$item='miscellaneous';
		$sql = "INSERT INTO s5_expenses(`item`,`price`,`purchased_date`,`user_id`,`client_id`) VALUES
		(:item,:price,:purchased_date,:user_id,:client_id)";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql); 
		    $stmt->bindParam("item", $item);
		    $stmt->bindParam("price", $data->price);
		    $stmt->bindParam("purchased_date", $data->purchased_date);   
		    $stmt->bindParam("user_id", $user_id);
		    $stmt->bindParam("client_id", $data->client_id);  
		    $stmt->execute();
		    $db = null;
		    return '{"msg": "expenses added","status":1}';
		} catch(PDOException $e) {
		    return '{"msg":"'.$e->getMessage().'","status":0}';
		}
	}
	// delete expenses based on id
	public function deleteExpenses($data){			
		$sql = "DELETE FROM s5_expenses WHERE id=:id";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("id", $data->id); 
			$stmt->execute();
			$count = $stmt->rowCount();
			$db = null;
			if($count>0){
				return '{"msg":"expenses deleted","status":1}';
			}
			else{
				return '{"msg":"opps something went wrong","status":0}';
			}
		} catch(PDOException $e) {
			return '{"msg":"'.$e->getMessage().'","status":0}';
		}	
		
	}


	/*
	* Get id based on user
	*
	*/
	public function getUserId($user){
		$sql = "SELECT id FROM s5_users WHERE user=:user";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("user", $user);
		    $stmt->execute();
		    $user_id = $stmt->fetchColumn();  
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    return ($rowCount>'0') ? $user_id
		    	:'{"msg": "No user found!","status":0}';
		  } catch(PDOException $e) {
		    return '{"msg":"'.$e->getMessage().'","status":0}';
		  }
	}

}

?>