<?php
require 'database.php';
class Users extends Database{	
	public function __construct(){}
	
	/*
	* It returns the users detail
	*/
	public function getUsers(){
		$db =$this->getConnection();
		$sql = "select * FROM s5_users ORDER BY user";
		try {
		    $stmt = $db->query($sql);  
		    $users = $stmt->fetchAll(PDO::FETCH_OBJ);
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    return ($rowCount>'0') ? '{"msg": ' . json_encode($users) . ',"status":1}'
		    	:'{"msg": "No user found!","status":0}';
	  	} catch(PDOException $e) {
	    	return '{"msg":"'.$e->getMessage().'","status":0}';
	  	}
	}

	/*
	* It returns a user detail by id
	*/
	public function getUser($id){
		$sql = "SELECT * FROM s5_users WHERE id=:id";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("id", $id);
		    $stmt->execute();
		    $user = $stmt->fetchObject();  
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    return ($rowCount>'0') ? '{"msg": ' . json_encode($user) . ',"status":1}'
		    	:'{"msg": "No user found!","status":0}';
		  } catch(PDOException $e) {
		    return '{"msg":"'.$e->getMessage().'","status":0}';
		  }
	}

	/*
	* It adds users
	*/
	public function addUsers($data){	
		$sql = "INSERT INTO s5_users(`user`,`pass`,`client_id`) VALUES
		(:user,:pass,:client_id)";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql); 
		    $stmt->bindParam("user", $data->user);
		    $stmt->bindParam("pass", $data->pass);
		    $stmt->bindParam("client_id", $data->client_id);
		    $stmt->execute();
		    $db = null;
		    return array("msg"=>"user added!","status"=>1);
		} catch(PDOException $e) {
		    return array("msg"=>$e->getMessage(),"status"=>0);
		}
	}

	/*
	* It checks username to login
	*/
	public function login($data){
		$sql = "SELECT user FROM s5_users WHERE user=:user AND pass=:pass AND client_id=:client_id";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("user", $data->user);
		    $stmt->bindParam("pass", $data->pass);
		    $stmt->bindParam("client_id", $data->client_id);
		    $stmt->execute();
		    $rowCount = $stmt->rowCount();// if exists 1 else 0
		    $db = null;
		    return ($rowCount==1)? array("msg"=>"user loggedIn!","status"=>1)
		    	:array("msg"=>"Oops, username or password doesn't exist!","status"=>0);
		  } catch(PDOException $e) {
		    return array("msg"=>$e->getMessage(),"status"=>0);
		  }
	}

}

?>