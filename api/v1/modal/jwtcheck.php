<?
class JWTcheck{
	public $token = array(
		"iat" => 1357000002,
		"nbf" => 1357000000
		);
	public function __construct(){}
	public static function checkJWT($jwt){
		try{
			$decoded = JWT::decode($jwt, KEY, array('HS256'));
			if(is_object($decoded))
				return true;
			exit;
		}
		catch(Exception $e){
			echo $e->getMessage();
			exit;
		}
	}
	public static function createJWT($user){
		$token = array(
			"iss"=>$user,
			"iat" => 1357000002,
			"nbf" => 1357000000
		);
		$jwt = JWT::encode($token, KEY);
		return $jwt;
	}

}