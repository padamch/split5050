<?php
require 'database.php';
class Clients extends Database{	
	public function __construct(){}
	
	/*
	* It returns the clients detail
	*/
	public function getClients(){
		$db =$this->getConnection();
		$sql = "select * FROM s5_clients ORDER BY email";
		try {
		    $stmt = $db->query($sql);  
		    $clients = $stmt->fetchAll(PDO::FETCH_OBJ);
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    return ($rowCount>'0') ? '{"msg": ' . json_encode($clients) . ',"status":1}'
		    	:'{"msg": "No client found!","status":0}';
	  	} catch(PDOException $e) {
	    	return '{"msg":"'.$e->getMessage().'","status":0}';
	  	}
	}

	/*
	* It returns a client detail by id
	*/
	public function getClient($id){
		$sql = "SELECT * FROM s5_clients WHERE id=:id";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("id", $id);
		    $stmt->execute();
		    $user = $stmt->fetchObject();  
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    return ($rowCount>'0') ? '{"msg": ' . json_encode($user) . ',"status":1}'
		    	:'{"msg": "No client found!","status":0}';
		  } catch(PDOException $e) {
		    return '{"msg":"'.$e->getMessage().'","status":0}';
		  }
	}

	/*
	* It adds users
	*/
	public function addClients($data){	
		$sql = "INSERT INTO s5_clients(`email`) VALUES
		(:email)";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql); 
		    $stmt->bindParam("email", $data->email);
		    $stmt->execute();
		    $id = $db->lastInsertId();
		    $db = null;
		    return array("msg"=>$id,"status"=>1);
		} catch(PDOException $e) {
		    return array("msg"=>$e->getMessage(),"status"=>0);
		}
	}

}

?>